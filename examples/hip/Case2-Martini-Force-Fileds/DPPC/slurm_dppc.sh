#!/bin/bash
#SBATCH -J dppc
#SBATCH -p normal
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --exclusive
#SBATCH --gres=dcu:1
#SBATCH -o %x-%j.log

#env|grep -E "SLURM|ROCM|HIP"

#conda deactivate

module purge                                                                    
module load compiler/devtoolset/7.3.1                                           
module load mpi/hpcx/2.7.4/gcc-7.3.1                                            
module load compiler/rocm/dtk/22.10.1
module load apps/anaconda3/2022.10
source activate py3q

date
#cd examples/hip/Case2-Martini-Force-Fileds/DPPC
rm -rf particles.*
python dppc.py --gpu=0
#python dppc_bilayer.py --gpu=0
