#!/bin/bash
#SBATCH -J dpd-binary
#SBATCH -p normal
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --exclusive
#SBATCH --gres=dcu:1
#SBATCH -o %x-%j.log

#env|grep -E "SLURM|ROCM|HIP"

#conda deactivate

module purge                                                                    
module load compiler/devtoolset/7.3.1                                           
module load mpi/hpcx/2.7.4/gcc-7.3.1                                            
module load compiler/rocm/dtk/22.10.1                                                
module load apps/anaconda3/2022.10
source activate py3q

date
#cd examples/hip/Case1-DPD/BinaryLiquid
rm -rf particles.dcd particles.*.xml particles.*.mol2
python dpd-binary-liquid.py --gpu=0
