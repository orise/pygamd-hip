# 东方超算系统GALAMOST/PYGAMD使用指南

文档版本v1.0，2023年4月22日，徐顺， xushun@sccas.cn

本指南主要面向基于C86平台的**东方超算系统**用户，也可作为其他平台用户参考。C86平台的CPU处理器兼容X86指令集，类GPU处理器DCU采用兼容AMD ROCM的DTK软件开发与运行环境。

# 一、概要

GALAMOST是一个面向聚合物、软物质、生物分子等体系的分子动力学(MD)模拟软件，实现了完全基于GPU的计算加速。一般而言，全原子或粗粒度力场不容易在MD包之间移植，用户很难在已经开发好的MD包中实现新方法，于是GALAMOST团队开发了PYGAMD （Python GPU-Accelerated Molecular Dynamics Software）MD模拟软件来解决这些问题。PYGAMD是一个MD计算软件平台，除集成了GALAMOST功能模块，还提供一个基于Python的分子动力学模拟解释器，从而支持用户无需修改底层代码来自定义力场，利用Numba库实现了GPU加速计算。PYGAMD是一个兼顾效率和易用性的多功能分子动力学模拟软件，具有较强的定制性，能够满足诸多领域的应用需求。当前PYGAMD软件包括MD引擎pygamd、分子构型生成器molgen和数据处理程序dataTackle等三大模块。

由于numba库尚不支持HIP加速，**因此PYGAMD中解析型力场的numba无法用DCU加速**，DCU只能用于PYGAMD中GALAMOST模块的计算加速。

- 官方网站: http://pygamd.com

- 代码库: https://github.com/youliangzhu/pygamd-v1

# 二、源代码说明

PYGAMD核心代码包括GALAMOST模块和numba解析模块，分别对应源代码中poetry和pygamd目录， peotry目录除了包括GALAMOST模块，还包括分子结构生成molgen和模拟后数据分析dataTackle两个插件。

使用之前推荐了解PYGAMD源代码目录：https://github.com/youliangzhu/pygamd-v1 ，相关源代码文档说明如下：

- [docs](https://github.com/youliangzhu/pygamd-v1/tree/main/docs) 为PYGAMD的使用文档，对应在线文档 https://pygamd-v1.readthedocs.io/en/latest/

- [examples](https://github.com/youliangzhu/pygamd-v1/tree/main/examples) 为PYGAMD使用例子，包括基于cuda和基于numba的两大类实例，cuda例子为之前GALAMOST的GPU加速实例，而numba例子使用PYGAMD的解析力场，通过numba调用GPU进行加速。

- [poetry](https://github.com/youliangzhu/pygamd-v1/tree/main/poetry) 主要为GALAMOST二进制动态编译库，分子产生器对应molgen.so，CUDA平台对应cu_gala.so，HIP平台对应hip_gala.so，还包括PYGAMD启动时调用的诗词集。

- [pygamd](https://github.com/youliangzhu/pygamd-v1/tree/main/pygamd) 为PYGAMD基于numba实现的解析力场的分子动力学模拟模块

- [test](https://github.com/youliangzhu/pygamd-v1/tree/main/test) 一些基本的集成测试实例，覆盖不同的版本

- [tutorials](https://github.com/youliangzhu/pygamd-v1/tree/main/tutorials) 为基于jupyter notebook的操作实例

- [setup.py](https://github.com/youliangzhu/pygamd-v1/blob/main/setup.py) PYGAMD软件包的安装脚本

需要注意：PYGAMD中的GALAMOST模块（对应peotry）代码**采用二级制动态库发布**，免去了基于CUDA和HIP的编译，在一定CUDA和HIP环境下可以直接运行PYGAMD的GALAMOST模块，因此对CUDA和HIP的版本有一定依赖。

# 三、软件安装


由于PYGAMD强依赖于Python环境， 具体依赖Python3下的numpy、numba、llvmlite和pybind11等packages，因此在C86平台推荐在conda环境下安装使用PYGAMD。Conda 是一个开源的软件包管理系统和环境管理系统，用于安装多个版本的软件包及其依赖关系，并在它们之间轻松切换。在C86平台上提供anaconda3的环境，为了代码运行环境相对独立，在本地创建一个conda环境，这里名为py3q，并在其中安装必要的Python3。当前PYGAMD版本依赖Python 3.9版。

```bash
module load apps/anaconda3/2022.10
#conda config --set auto_activate_base false
#conda deactivate
conda create -n py3q python=3.9 #指定3.9版本，自动在本地$HOME/.conda目录下创建
#conda activate py3q
source activate py3q
#conda env list
#where pip
```

以上过程需要服务器需要连接外网。在东方超算系统默认关闭HTTP外网访问，推荐通过SSH转发HTTP代理端口来连外网，参考集群[无法访问外网HTTP服务](https://gitlab.com/xushun/linux/shell/-/blob/master/cluster_faq.md#2%E6%97%A0%E6%B3%95%E8%AE%BF%E9%97%AE%E5%A4%96%E7%BD%91http%E6%9C%8D%E5%8A%A1)的FAQ文档进行解决。

通过以上执行命令创建了一个名为py3q基于Python 3的conda环境。默认创建在本地$HOME/.conda/py3q目录下。这里py3q名字可自定义，只需和接下来所涉及的操作保持名称一致即可。在conda-forge通道提供了很多Python常用库，可以直接通过conda install命令安装。对于有些conda-forge不存在的Python库，或者基于setup.py的Python源代码软件包，推荐使用pip来安装。这里推荐使用pip安装PYGAMD软件包。

## 使用pip在线安装

在东方超算系统安装conda python3环境之后（这里命名为py3q），激活该环境，并使用pip install安装PYGAMD，这个过程也需要联网。

```bash
[xushun@login10 ~]$ module load apps/anaconda3/2022.10
[xushun@login10 ~]$ source activate py3q

(py3q) [xushun@login10 ~]$ which pip
~/.conda/envs/py3q/bin/pip
(py3q) [xushun@login10 ~]$ pip install pygamd
Collecting pygamd
  Downloading pygamd-1.2.8-py3-none-any.whl (25.8 MB)
# ... 此处省略多行信息
Installing collected packages: numpy, llvmlite, numba, pygamd
  Attempting uninstall: numpy
    Found existing installation: numpy 1.24.2
    Uninstalling numpy-1.24.2:
      Successfully uninstalled numpy-1.24.2
Successfully installed llvmlite-0.39.1 numba-0.56.4 numpy-1.23.5 pygamd-1.2.8
```

 可以看到PYGAMD安装过程并没有调用HIP进行代码实时编译，只是进行了已编译的动态库的拷贝，查看安装后查看PYGAMD软件包的两个重要目录poetry和pygamd如下：


```bash
[xushun@login10 ~]$ ls $HOME/.conda/envs/py3q/lib/python3.9/site-packages/poetry/
cu_gala.so  dataTackle  force_field_gala.py  force_field_itp.py  gala_to_py3.py  gro_to_xml.py  hip_gala.so  __init__.py  molgen.so  

[xushun@login10 ~]$ ls $HOME/.conda/envs/py3q/lib/python3.9/site-packages/pygamd
application.py  chare.py  chares  dump.py  force.py  forces  __init__.py  integration.py  integrations  plist.py  plists  __pycache__
```

使用以下命令升级pygamd版本

```
pip install --upgrade pygamd
```

## 使用pip离线安装

由于东方超算无法直接访问外网，也可以采用上传whl软件包的手动安装方式。PYGAMD所依赖的package包括但不限于numba、numpy、llvmlite和pybind11；本次安装的pygamd 1.28版本依赖numba-0.56.4、numpy-1.23.5、llvmlite-0.39.1和pybind11-2.7.1。可以在网站`https://pypi.org/`手动下载PYGAMD的[whl文件](https://pypi.org/project/pygamd/#files)和依赖的whl文件。简便的方式是通过生成requirements.txt，使用pip自动下载和安装各个whl文件。可以通过已安装过PYGAMD的pip 环境生成requirements.txt文件

```bash
module purge
module load apps/anaconda3/2022.10
source activate py3q
pip freeze |grep -E "numba|numpy|pybind11|llvmlite|pygamd" >requirements.txt
```

requirements.txt文件如下
```
llvmlite==0.39.1
numba==0.56.4
numpy==1.23.5
pybind11==2.7.1
pygamd==1.2.8
```
如果没有安装过PYGAMD的pip环境，也可以手动填写以上内容。

通过pip下载所需的依赖库到指定目录`$HOME/pygamd-whls`

```
pip download -d $HOME/pygamd-whls -r requirements.txt
```
本文档附有[pygamd-whls](https://gitlab.com/orise/pygamd-hip/-/tree/master/pygamd-whls)目录中whl文件，可直接下载。

加载安装和运行的环境，这个环境包括GCC、MPI和DTK，安装PYGAMD软件包不需要这些环境，但安装PYGAMD软件依赖库可能需要。

```bash
module purge
module load compiler/devtoolset/7.3.1
module load mpi/hpcx/2.7.4/gcc-7.3.1
module load compiler/rocm/dtk/22.10.1
module load apps/anaconda3/2022.10
source activate py3q
```
传入pip依赖库列表文件requirements.txt，可实现自动安装PYGAMD及其依赖库
```
pip install --no-index --find-links=$HOME/pygamd-whls -r requirements.txt
```

`–find-links` 后面跟的是存放 whl 文件包的路径；也可以使用pip install进入pygamd-whls目录，安装各个whl软件包

```
pip install <filename>.whl
```

 

# 四、实例测试

## 环境配置

在PYGAMD安装之后，poetry模块安装在`$HOME/.conda/envs/py3q/lib/python3.9/site-packages/poetry`；而pygamd模块安装在`$HOME/.conda/envs/py3q/lib/python3.9/site-packages/pygamd`。pygamd模块在C86平台无法使用。在C86平台执行`import pygamd`，会产生CUDA报错：

```
numba.cuda.cudadrv.error.CudaSupportError: Error at driver init:

CUDA driver library cannot be found.
If you are sure that a CUDA driver is installed,
try setting environment variable NUMBA_CUDA_DRIVER
with the file path of the CUDA driver shared library.
```

产生错误的原因是pygamd当前使用numba.cuda模块，而这个模块在C86的DTK环境并不支持。因此**PYGAMD软件在C86平台只能使用peotry模块**，也即GALAMOST核心模块。

为了方便使用GALAMOST功能，新建路径变量`GALA_PATH`指向peotry安装目录

```
GALA_PATH=$HOME/.conda/envs/py3q/lib/python3.9/site-packages/poetry
```

这个变量可以在shell中通过export进行全局定义，也可以使用conda将其定义在py3q环境下，操作如下

```bash
module load apps/anaconda3/2022.10
source activate py3q
conda env config vars set GALA_PATH=$HOME/.conda/envs/py3q/lib/python3.9/site-packages/poetry
#conda env config vars list
```

在安装目录$GALA_PATH中，hip_gala.so和cu_gala.so等动态库依赖系统很多动态库，如`libpython3.so`。使用ldd查看hip_gala.so的所有依赖：

```
ldd $GALA_PATH/hip_gala.so
```

看看其所有依赖的库是否都能够找到对应的文件路径。如果找不到`libpython3.so`，可以指向py3q环境下的libpython3.so，使用conda定义其在py3q环境下

```bash
#module load apps/anaconda3/2022.10
#source activate py3q
conda env config vars set LD_LIBRARY_PATH=$HOME/.conda/envs/py3q/lib:$LD_LIBRARY_PATH
#conda env config vars list
```

使用`conda env config vars`命令可在py3q环境下定义变量，保存在文件`$HOME/.conda/envs/py3q/conda-meta/state`，可以使用unset命令删除已定义的变量，如

```
conda env config vars unset LD_LIBRARY_PATH
```

或者直接删除conda-meta/state文件。

PYGAMD安装目录提供了dataTackle命令进行模拟后数据的分析，需要为其添加可执行权限，方便调用执行。

```
chmod +x $GALA_PATH/dataTackle
```

在`$GALA_PATH`目录中的`force_field_gala.py`、`force_field_itp.py`和`gala_to_py3.py`等文件默认使用cu_gala

```bash
cd $GALA_PATH
grep cu_gala *.py
```
输出如下查找内容：
```
force_field_gala.py:from poetry import cu_gala as gala
force_field_itp.py:from poetry import cu_gala as gala
gala_to_py3.py:                outgala.write("from poetry import cu_gala as gala\n")
```

在C86平台需要改为hip_gala，**在每次升级之后也需要更改hip_gala**，使用如下命令操作

```bash
cd $GALA_PATH
sed -i "s/cu_/hip_/g" *.py
```

下载C86平台上的测试实例

```
cd ~
git clone https://gitlab.com/orise/pygamd-hip.git
```

其中`pygamd-hip/examples`是已适配C86环境的实例文件。

## DPD实例

在PYGAMD的git库中提供了examples目录，可以将其下载来测试。如`Case1-DPD/BinaryLiquid`实例的下载地址为https://github.com/youliangzhu/pygamd-v1/tree/main/examples/cuda/Case1-DPD/BinaryLiquid 。其中包含文件

- dpd-binary-liquid.molg 分子体系生成脚本，仅使用CPU执行
- dpd-binary-liquid.py 分子体系的模拟脚本，需要DCU加速

PYGAMD的分子体系生成过程是独立于体系模拟过程的，一般过程为：首先调用Python的molgen模块进行分子体系生成，保存在xml文件，其次调用Python的poetry中的cu_gala或hip_gala模块进行体系模拟。

执行`dpd-binary-liquid.molg `脚本，计算量较小，计算时间较短，也不涉及异构加速，因此无需通过作业系统提交作业，可以在登录节点上直接执行，如：

```bash
module load apps/anaconda3/2022.10
source activate py3q
python dpd-binary-liquid.molg
```

注意需要在安装了PYGAMD的python环境下（这里py3q）运行molg脚本。这个脚本会生成`AB.xml`构象文件，供`dpd-binary-liquid.py`脚本读取。

`dpd-binary-liquid.py`默认是调用CUDA加速计算，在HIP下执行需修改代码，将`from poetry import cu_gala as gala`替换为`from poetry import hip_gala as gala`; 或者使用命令行替换操作

```
sed -i "s/cu_/hip_/g" dpd-binary-liquid.py
```

> 注意：PYGAMD相对早期版本的GALAMOST软件的运行脚本有部分命名方式的修改，早期的GALAMOST运行脚本可以通过PYGAMD提供的`$GALA_PATH/gala_to_py3.py`脚本进行自动更新。只要进入需要转换文件的目录，执行
>
> ```
> python $GALA_PATH/gala_to_py3.py
> ```
>
> 该脚本会搜索当前目录所有`.gala`和`.molg`文件，进行文件内容相应的关键字替换，并将`.gala`文件重命名为`.py`文件，`*.molg`文件命名为`*_new.molg`文件。

为了执行`dpd-binary-liquid.py`，新建脚本`slurm_dpd.sh`如下

```bash
#!/bin/bash
#SBATCH -J dpd-binary
#SBATCH -p normal
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --exclusive
#SBATCH --gres=dcu:1
#SBATCH -o %x-%j.log

#env|grep -E "SLURM|ROCM|HIP"

module purge                                                                    
module load compiler/devtoolset/7.3.1                                           
module load mpi/hpcx/2.7.4/gcc-7.3.1                                            
module load compiler/rocm/dtk/22.10.1                                                
module load apps/anaconda3/2022.10
source activate py3q

date
#cd examples/hip/Case1-DPD/BinaryLiquid
python dpd-binary-liquid.py --gpu=0
```

使用命令`sbatch slurm_dpd.sh`进行作业提交。注意：使用sbatch命令提交以上作业时，需要调用`conda deactivate`命令退出py3q环境；`-p normal`选项中`normal`为当前用户可用分区，当前`hip_gala`只支持串行单DCU加速，这里使用`--gres=dcu:1`只申请了一块DCU卡用于计算，GPU设备编号从0开始。

可查看日志文件`dpd-binary-***.log`确认是否运行成功。运行成功后会输出统计变量文件`data.log`、体系snapshot文件`particles.***.xml`和轨迹文件`particles.dcd`等等。可以调用`dataTrackle`进行模拟后的轨迹分析，命令如：

```
$GALA_PATH/dataTackle particles.0000000000.xml particles.dcd
```

##  DPPC实例

在PYGAMD的git库中提供了`Case2-Martini-Force-Fileds/DPPC`实例，其下载地址为https://github.com/youliangzhu/pygamd-v1/tree/main/examples/cuda/Case2-Martini-Force-Fileds/DPPC 。其中包含两个子实例：

- 实例1：dppc.py、dppc.molg
- 实例2：dppc_bilayer.py、dppc_bilayer.xml 

将其中各文件中对cu_gala的调用，改为对hip_gala的调用，以适配C86平台。

为了执行`dppc.py`或`dppc_bilayer.py`，新建脚本`slurm_dppc.sh`如下

```bash
#!/bin/bash
#SBATCH -J dppc
#SBATCH -p normal
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --exclusive
#SBATCH --gres=dcu:1
#SBATCH -o %x-%j.log

#env|grep -E "SLURM|ROCM|HIP"

module purge
module load compiler/devtoolset/7.3.1
module load mpi/hpcx/2.7.4/gcc-7.3.1
module load compiler/rocm/dtk/22.10.1
module load apps/anaconda3/2022.10
source activate py3q

date
#cd examples/hip/Case2-Martini-Force-Fileds/DPPC
rm -rf particles.*
#python dppc.py --gpu=0
python dppc_bilayer.py --gpu=0
```

使用命令`sbatch slurm_dppc.sh`进行作业提交，这里`dppc.py`和`dpcc_bilayer.py`二者选一执行。两个实例运行的Avage TPS指标为

| Case            | Average TPS |
| --------------- | ----------- |
| dppc.py         | 1627.99     |
| dppc_bilayer.py | 1677.86     |




## 参考链接

- PYGAMD的说明文档 http://pygamd.com/index.php?m=content&c=index&a=lists&catid=2
- GALAMOST实例 https://github.com/youliangzhu/pygamd-v1/tree/main/examples/cuda
- Slurm资源管理与作业调度系统安装配置 http://hmli.ustc.edu.cn/doc/linux/slurm-install/index.html
- SLURM 使用参考 http://faculty.bicmr.pku.edu.cn/~wenzw/pages/slurm.html
